﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hackathon_2021
{

    public partial class Ending : Window
    {
        string endingHeader;
        string endingConclusion;

        public Ending()
        {
            InitializeComponent();

            this.DataContext = Start.user;
            this.DataContext = MainWindow.hasAge;
            this.DataContext = MainWindow.hasSchool;
            this.DataContext = MainWindow.hasName;
            this.DataContext = MainWindow.hasBlocked;
            this.DataContext = MainWindow.information;
            this.DataContext = MainWindow.wantsToMeet;
            GenerateEnding();
            headerText.Text = endingHeader;
            conslusionText.Text = endingConclusion;
        }

        private void GenerateEnding()
        {
            if(!MainWindow.hasName && !MainWindow.hasAge && MainWindow.information < 2)
            {
                endingHeader = "GLÜCKWUNSCH";
                endingConclusion = "Du hast die Simulation erfolgreich beendet und nicht zu viele Informationen über dich preisgegeben, wodurch du dich nicht in Gefahr begeben hast. ";
            }          
            
            if(!MainWindow.hasName && !MainWindow.hasAge && MainWindow.information >= 2)
            {
                endingHeader = "GLÜCKWUNSCH";
                endingConclusion = "Du hast die Simulation erfolgreich beendet und nicht zu viele Informationen über dich preisgegeben, wodurch du dich nicht in Gefahr begeben hast. ";
                endingConclusion += Environment.NewLine + Environment.NewLine + "Dennoch solltest du auch mit unverfänglichen Informationen vertraulich umgehen, da dein Gesprächspartner diese dazu nutzen " +
                              "könnte, um dieselben Probleme/Interessen vorzutäuschen, um dein Vertrauen zu gewinnen. ";
            }

            if ((MainWindow.hasAge || MainWindow.hasName) && MainWindow.information < 5)
            {
                endingHeader = "DAS SIEHT SCHON GANZ GUT AUS";
                endingConclusion = "Du hast die Simulation beendet und nicht zu viele relevante Informationen über dich preisgegeben, wodurch du dich nur ein wenig in Gefahr begeben hast. ";
                endingConclusion += Environment.NewLine + Environment.NewLine + "Dennoch solltest du auch mit unverfänglichen Informationen vertraulich umgehen, da dein Gesprächspartner diese dazu nutzen " +
                                    "könnte, um dieselben Probleme/Interessen vorzutäuschen, um dein Vertrauen zu gewinnen. ";
            }

            if (MainWindow.hasAge || MainWindow.hasName)
            {
                endingHeader = "DAS SIEHT SCHON GANZ GUT AUS";
                endingConclusion = "Du hast die Simulation beendet und nicht zu viele relevante Informationen über dich preisgegeben, wodurch du dich nur ein wenig in Gefahr begeben hast. ";
                endingConclusion += Environment.NewLine + Environment.NewLine + "Dennoch solltest du auch mit unverfänglichen Informationen vertraulich umgehen, da dein Gesprächspartner diese dazu nutzen " +
                                    "könnte, um dieselben Probleme/Interessen vorzutäuschen, um dein Vertrauen zu gewinnen. ";
            }

            if (MainWindow.hasName && MainWindow.hasAge || MainWindow.hasSchool && MainWindow.hasAge || MainWindow.information > 5)
            {
                endingHeader = $"{Start.user.Name.ToUpper()} ({Start.user.Age}) WIRD VERMISST";
                endingConclusion = "Du hast die Simulation beendet und viele relevante Informationen über dich preisgegeben, wodurch du dich in Gefahr begeben hast. ";
                endingConclusion = Environment.NewLine + Environment.NewLine + "Es ist sehr gefährlich dein richtiges Alter und deinen richtigen Namen oder deine Schule im Internet Fremden gegenüber preiszugeben ";
                endingConclusion += Environment.NewLine + Environment.NewLine  + "Auch solltest du auch mit unverfänglichen Informationen vertraulich umgehen, da dein Gesprächspartner diese dazu nutzen " +
                        "könnte, um dieselben Probleme/Interessen vorzutäuschen, um dein Vertrauen zu gewinnen. ";
            }
            if (MainWindow.wantsToMeet)
            {
                endingHeader = $"{Start.user.Name.ToUpper()} ({Start.user.Age}) WIRD VERMISST";
                endingConclusion += Environment.NewLine + Environment.NewLine + "Es ist sehr leichtfertig, sich mit Fremden Menschen aus dem Internet zu treffen, dies ist sehr gefährlich für dich," +
                    " da man nie weiß, wer sich in Wirklichkeit hinter dem Gesprächspartner verbirgt und welche Absichten er hat. ";
            }

            if (!MainWindow.hasBlocked)
            {
                endingConclusion += Environment.NewLine + Environment.NewLine + "Um Gesprächspartner zu meiden, die dir nicht vertrauenswürdig vorkommen, " +
                                    "hättest du auch die Blockierfunktion nutzen können, die sich oben rechts unter dem Symbol mit den drei vertikalen Punkten befindet";
            }
        }

        private void restart_Click(object sender, RoutedEventArgs e)
        {
            Start start = new Start();
            start.Show();
            Close();
        }
    }
}
