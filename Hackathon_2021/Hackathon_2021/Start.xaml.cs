﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hackathon_2021
{
    public partial class Start : Window
    {
        public static User user = new User { Name = "Name", Age = "Alter" };

        public Start()
        {
            InitializeComponent();
            this.DataContext = user;
        }

        private void btn_signIn_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
     
        }
    }
} 

