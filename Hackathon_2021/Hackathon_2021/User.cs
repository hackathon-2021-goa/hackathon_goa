﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hackathon_2021
{
    //Auslagern oder maybe hier lassen oder Vereinfachen, weil Objekt nicht zwingend nötig, glaub ich 
    public class User
    {

        private string nameValue;

        public string Name
        {
            get { return nameValue; }
            set { nameValue = value; }
        }

        private string ageValue;

        public string Age
        {
            get { return ageValue; }

            set
            {
                if (value != ageValue)
                {
                    ageValue = value;
                }
            }
        }


    }
}
