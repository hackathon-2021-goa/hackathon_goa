﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hackathon_2021
{

    public partial class MainWindow : Window
    {
        static bool FirstTime = true;
        static List<int> decisionsScenario1 = new List<int>();
        List<int> decisionsScenario2 = new List<int>();
        List<int> decisionsScenario3 = new List<int>();
        string[,] hobbyArray =
        {
            {"Freunde treffen", "treffe auch gern Freunde", "uns ja auch mal treffen",  "Zeit mit Freunden verbracht", "Hausarrest" },
            {"Fußball spielen", "spiele auch gern Fußball", "ja mal zusammen kicken",  "Fußball gespielt", "Hausarrest" },
            {" zocken", "zocke auch gern", "ja zusammen zocken",  "gezockt", "Gamingverbot" }
        };

        private string[] subjectArray = { "Mathe", "Deutsch", "Englisch" };

        int hobby;
        int subject;
        public static bool hasName;
        public static bool hasAge;
        public static bool hasBlocked;
        public static bool hasSchool;
        public static bool wantsToMeet;
        public static int information = 0;

        bool isScenario1Blocked = false;
        bool isScenario2Blocked = false;
        bool isScenario3Blocked = false;

        public MainWindow()
        {
            InitializeComponent();

            if (FirstTime == true)
            {
                FirstTime = false;
                OpenedFirstTime();
            }

            else
            {
                OpenedSecondTime();
            }

            this.DataContext = Start.user;
            _user_name.Text = Start.user.Name;
            _btn_blockUser.Text = "Blockieren";
            _userBlocked.Visibility = Visibility.Hidden;
            Reset();
            HideOptions();
        }

        private void OpenedFirstTime()
        {
            //Öffnet den Login-Bildschirm
            var start = new Start();
            start.Show();
            Close();
        }

        private void OpenedSecondTime()
        {
            //Priorität: low 
            //Soll gespeichterten Stand zeigen 
            //Wenn Stand gespeichtert werden können soll 
        }

        private void DisplayScenario1()
        {
            ShowOptions();

            string chatpartner = _chatpartner_name.Text + ":" + Environment.NewLine;
            string user = "Du:" + Environment.NewLine;
            chat.Text = "";
            chat.Text = chatpartner + "hii :)" + Environment.NewLine + Environment.NewLine;
            chat.Text += user + "Hallo :)" + Environment.NewLine + Environment.NewLine;
            chat.Text += chatpartner + "wie heißt du denn" + Environment.NewLine + "?" + Environment.NewLine + Environment.NewLine;
            _btn_option1.Content = Start.user.Name;
            _btn_option2.Content = "Sag ich nicht...";
            _btn_option3.Content = "Wieso fragst du?";

            if (decisionsScenario1.Count > 0)
            {
                //Entscheidung 1
                switch (decisionsScenario1[0])
                {
                    case 1:
                        hasName = true;
                        information++;
                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                        chat.Text += chatpartner + $"freut mich, dich kennenzulernen {Start.user.Name}" + Environment.NewLine + ":)" + Environment.NewLine + "ich bin lena" + Environment.NewLine + Environment.NewLine;
                        if (decisionsScenario1.Count == 1)
                        {
                            decisionsScenario1.Add(666);
                        }
                        break;

                    case 2:
                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                        chat.Text += chatpartner + "oh, schade" + Environment.NewLine + ":(" + Environment.NewLine + Environment.NewLine;
                        if (decisionsScenario1.Count == 1)
                        {
                            decisionsScenario1.Add(666);
                        }
                        break;

                    case 3:
                        chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                        chat.Text += chatpartner + "war nur neugierig" + Environment.NewLine + "ich bin lena" + Environment.NewLine + ":)" + Environment.NewLine + Environment.NewLine;
                        _btn_option1.Content = $"Achso, ich bin {Start.user.Name} :)";
                        _btn_option2.Content = "Sag ich dir trotzdem nicht";
                        _btn_option3.Visibility = Visibility.Hidden;
                        if (decisionsScenario1.Count >= 2)
                        {
                            //Entscheidung 2
                            switch (decisionsScenario1[1])
                            {
                                case 1:
                                    hasName = true;
                                    information++;
                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                    chat.Text += chatpartner + $"freut mich, dich kennenzulernen {Start.user.Name}" + Environment.NewLine + ":)" + Environment.NewLine + Environment.NewLine;
                                    break;
                                case 2:
                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                    chat.Text += chatpartner + "oh, schade" + Environment.NewLine + ":(" + Environment.NewLine + Environment.NewLine;
                                    break;
                            }
                        }
                        break;
                }
            }

            if (decisionsScenario1.Count >= 2)
            {
                chat.Text += chatpartner + "wie gehts dir so?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Gut, dir?";
                _btn_option2.Content = "Geht so...";
                _btn_option3.Visibility = Visibility.Hidden;

                if (decisionsScenario1.Count >= 3)
                {
                    //Entscheidung 3
                    switch (decisionsScenario1[2])
                    {
                        case 1:
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "auch gut" + Environment.NewLine + ":)" + Environment.NewLine + Environment.NewLine;
                            if (decisionsScenario1.Count == 3)
                            {
                                decisionsScenario1.Add(666);
                            }
                            break;
                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "oh, was denn los?" + Environment.NewLine + ":(" + Environment.NewLine + Environment.NewLine;
                            _btn_option1.Content = "Hab den letzten Mathetest verhauen :(";
                            _btn_option2.Content = "Hab Hausarrest :(";
                            _btn_option3.Visibility = Visibility.Visible;
                            _btn_option3.Content = "Ist egal";

                            if (decisionsScenario1.Count >= 4)
                            {
                                //Entscheidung 4
                                switch (decisionsScenario1[3])
                                {
                                    case 1:
                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "oh man :(" + Environment.NewLine + Environment.NewLine;
                                        break;
                                    case 2:
                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "oh man :(" + Environment.NewLine + Environment.NewLine;
                                        break;
                                    case 3:
                                        chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "oh, ok :(" + Environment.NewLine + "hoffe trotzdem es wird bald wieder besser :)" + Environment.NewLine + Environment.NewLine;
                                        break;
                                }
                            }
                            break;
                    }
                }
            }

            if (decisionsScenario1.Count >= 4)
            {
                chat.Text += chatpartner + "wie alt bist du eig" + Environment.NewLine + "?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = $"{Start.user.Age}";
                _btn_option2.Content = "Wie alt bist du denn?";
                _btn_option3.Content = "Will ich nicht sagen";
                _btn_option3.Visibility = Visibility.Visible;

                //Entscheidung 5
                if (decisionsScenario1.Count >= 5)
                {
                    switch (decisionsScenario1[4])
                    {
                        case 1:
                            information++;
                            hasAge = true;
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "omg" + Environment.NewLine + $"ich bin auch {Start.user.Age} :)" + Environment.NewLine + Environment.NewLine;
                            if (decisionsScenario1.Count == 5)
                            {
                                decisionsScenario1.Add(666);
                            }
                            break;

                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "ich bin 12" + Environment.NewLine + ":)" + Environment.NewLine + Environment.NewLine;

                            _btn_option1.Content = $"Ich bin {Start.user.Age}";
                            _btn_option2.Content = "Okay";
                            _btn_option3.Content = "";
                            _btn_option3.Visibility = Visibility.Hidden;

                            //Entscheidung 6
                            if (decisionsScenario1.Count >= 6)
                            {
                                switch (decisionsScenario1[5])
                                {
                                    case 1:
                                        hasAge = true;
                                        information++;
                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                        break;
                                    case 2:
                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                        break;
                                }
                            }
                            break;

                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "hm ok :(" + Environment.NewLine + Environment.NewLine;
                            if (decisionsScenario1.Count == 5)
                            {
                                decisionsScenario1.Add(666);
                            }
                            break;
                    }
                }
            }

            if (decisionsScenario1.Count >= 6)
            {
                chat.Text += chatpartner + "was sind so deine hobbies" + Environment.NewLine + "?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Freunde treffen";
                _btn_option2.Content = "Fußball spielen";
                _btn_option3.Content = "Zocken";
                _btn_option3.Visibility = Visibility.Visible;

                //Entscheidung7
                if (decisionsScenario1.Count >= 7)
                {
                    switch (decisionsScenario1[6])
                    {
                        case 1:
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            hobby = 0;
                            break;
                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            hobby = 1;
                            break;
                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            hobby = 2;
                            break;
                    }
                }
            }

            if (decisionsScenario1.Count >= 7)
            {
                chat.Text += chatpartner + "cool!" + Environment.NewLine + $"ich {hobbyArray[hobby, 1]}"
                           + Environment.NewLine + $"können {hobbyArray[hobby, 2]}" + Environment.NewLine + ";)" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Gerne!";
                _btn_option2.Content = "Mal schauen :)";
                _btn_option3.Content = "Lieber nicht...";

                //Entscheidung 8
                if (decisionsScenario1.Count >= 8)
                {
                    switch (decisionsScenario1[7])
                    {
                        case 1:
                            information++;
                            information++;
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            break;
                        case 2:
                            information++;
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            break;
                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            break;
                    }
                }
            }

            if (decisionsScenario1.Count >= 8)
            {
                chat.Text += chatpartner + $"hast du in letzter Zeit viel {hobbyArray[hobby, 3]}?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Ne, hatte da leider nicht so viel Zeit für :(";
                _btn_option2.Content = "Klar :)";
                _btn_option3.Content = "Geht so";

                if (decisionsScenario1.Count >= 9)
                {
                    //Entscheidung 9
                    switch (decisionsScenario1[8])
                    {
                        case 1:
                            information++;
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            break;
                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "sehr gut :)" + Environment.NewLine + Environment.NewLine;
                            if (decisionsScenario1.Count == 9)
                            {
                                decisionsScenario1.Add(666);
                            }
                            break;
                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            break;
                    }

                    if (decisionsScenario1[8] == 1 || decisionsScenario1[8] == 3)
                    {
                        chat.Text += chatpartner + "oh :(" + Environment.NewLine + "wieso denn das?" + Environment.NewLine + Environment.NewLine;
                        _btn_option1.Content = "Hausaufgaben :(";
                        _btn_option2.Content = $"Hab {hobbyArray[hobby, 4]} :(";
                        _btn_option3.Content = "Nicht so wichtig";

                        if (decisionsScenario1.Count >= 10)
                        {
                            //Entscheidung 10
                            switch (decisionsScenario1[9])
                            {
                                case 1:
                                    information++;
                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                    chat.Text += chatpartner + "hausaufgaben sind blöd :(" + Environment.NewLine + Environment.NewLine;
                                    break;
                                case 2:
                                    information++;
                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                    chat.Text += chatpartner + "ugh, eltern nerven so hart :(" + Environment.NewLine
                                               + "tut mir leid für dich" + Environment.NewLine + Environment.NewLine;
                                    break;
                                case 3:
                                    chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                                    chat.Text += chatpartner + "hm okay :(" + Environment.NewLine + Environment.NewLine;
                                    break;
                            }
                        }
                    }
                }
            }
            if (decisionsScenario1.Count >= 10)
            {
                chat.Text += chatpartner + "bekommst du eigentlich immer so viele hausaufgaben auf" + Environment.NewLine + "?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Ja :(";
                _btn_option2.Content = "Geht so";
                _btn_option3.Content = "Ne";

                if (decisionsScenario1.Count >= 11)
                {
                    //Entscheidung 11
                    switch (decisionsScenario1[10])
                    {
                        case 1:
                            information++;
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "hausaufgaben sind blöd :(" + Environment.NewLine + Environment.NewLine;
                            _btn_option1.Content = "Am längsten sitze ich immer an Mathe :(";
                            _btn_option2.Content = "Am längsten sitze ich immer an Deutsch :(";
                            _btn_option3.Content = "Am längsten sitze ich immer an Englisch :(";

                            if (decisionsScenario1.Count >= 12)
                            {
                                //Entscheidung 12
                                switch (decisionsScenario1[11])
                                {
                                    case 1:
                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                        subject = 0;
                                        break;
                                    case 2:
                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                        subject = 1;
                                        break;
                                    case 3:
                                        chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                                        subject = 2;
                                        break;
                                }

                                chat.Text += chatpartner + $"{subjectArray[subject]} ist echt blöd :(" + Environment.NewLine
                                           + "aber können uns ja mal treffen und zusammen hausaufgaben machen" + Environment.NewLine + "zusammen geht das sicher besser" + Environment.NewLine + Environment.NewLine;
                                _btn_option1.Content = "Das wäre voll cool :)";
                                _btn_option2.Content = "Mal schauen...";
                                _btn_option3.Content = "Lieber nicht";

                                if (decisionsScenario1.Count >= 13)
                                {
                                    //Entscheidung 13
                                    switch (decisionsScenario1[12])
                                    {
                                        case 1:
                                            information++;
                                            information++;
                                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                            chat.Text += chatpartner + $"dann hättest du auch wieder mehr zeit für {hobbyArray[hobby, 0]}" + Environment.NewLine + Environment.NewLine;
                                            chat.Text += Environment.NewLine + Environment.NewLine + "Stimmt :)" + Environment.NewLine + Environment.NewLine;
                                            break;
                                        case 2:
                                            information++;
                                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                            chat.Text += chatpartner + $"dann hättest du auch wieder mehr zeit für {hobbyArray[hobby, 0]}" + Environment.NewLine + Environment.NewLine;
                                            chat.Text += Environment.NewLine + Environment.NewLine + "Stimmt :)" + Environment.NewLine + Environment.NewLine;
                                            break;
                                        case 3:
                                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                                            chat.Text += chatpartner + "schade :(" + Environment.NewLine + Environment.NewLine;
                                            break;
                                    }
                                    HideOptions();
                                }
                            }
                            break;
                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "du hast ein glück..." + Environment.NewLine + Environment.NewLine;
                            HideOptions();
                            break;
                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "du hast ein glück..." + Environment.NewLine + Environment.NewLine;
                            HideOptions();
                            break;
                    }
                    var ending = new Ending();
                    ending.Show();
                    Close();
                }
            }
            chat.ScrollToEnd();
        }

        private void DisplayScenario2()
        {
            _btn_option1.Visibility = Visibility.Visible;
            _btn_option2.Visibility = Visibility.Visible;
            _btn_option3.Visibility = Visibility.Hidden;
            _btn_option1.Content = "Kommt drauf an...Warum hast du mich denn angeschrieben?";
            _btn_option2.Content = "Ach, alles ok :)";
            string chatpartner = _chatpartner_name.Text + ":" + Environment.NewLine;
            string user = "Du:" + Environment.NewLine;
            chat.Text = "";
            chat.Text = chatpartner + "Heyy :D" + Environment.NewLine + Environment.NewLine;
            chat.Text = user + "Hii :)" + Environment.NewLine + Environment.NewLine;
            chat.Text += chatpartner + "Ich hoffe es stört dich nicht, dass ich dich einfach so angeschrieben habe ^^'" + Environment.NewLine + Environment.NewLine;

     
 

            if (decisionsScenario2.Count > 0)
            {
                //Entscheidung 1
                switch (decisionsScenario2[0])
                {
                    case 1:
                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                        chat.Text += chatpartner + "Ehrlicher Weise ist es so, dass ich vor kurzen Umgezogen bin und hab jetzt kaum noch Kontakt zu meinen alten Freunden..." + Environment.NewLine + "Deswegen dachte ich, schreib ich hier mal ein paar Leute an. Einfach um zu plaudern oder so." + Environment.NewLine + Environment.NewLine;

                        break;

                    case 2:
                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                        chat.Text += chatpartner + "Ehrlicher Weise ist es so, dass ich vor kurzen Umgezogen bin und hab jetzt kaum noch Kontakt zu meinen alten Freunden..." + Environment.NewLine + "Deswegen dachte ich, schreib ich hier mal ein paar Leute an. Einfach um zu plaudern oder so." + Environment.NewLine + Environment.NewLine;

                        break;
                }
            }
            if (decisionsScenario2.Count >= 1)
            {
                chat.Text += chatpartner + "Also.. was hast du so für Hobbys?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Das ist mir zu privat, sorry";
                _btn_option2.Content = "Ich lese gerne. Und du so?";
                _btn_option3.Visibility = Visibility.Hidden;

                if (decisionsScenario2.Count >= 2)
                {
                    //Entscheidung 2
                    switch (decisionsScenario2[1])
                    {
                        case 1:
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "Oh, das respektiere ich. Tut mir leid falls ich dir zu nahe getreten bin." + Environment.NewLine + "Wie alt bist du eigentlich wenn ich das Fragen darf?" + Environment.NewLine + Environment.NewLine;
                            _btn_option1.Content = $"Ich bin {Start.user.Age}!";
                            _btn_option2.Content = "Das geht dich nichts an...";
                            _btn_option3.Visibility = Visibility.Hidden;

                            if (decisionsScenario2.Count >= 3)
                            {
                                //Entscheidung 3
                                switch (decisionsScenario2[2])
                                {
                                    case 1:
                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "Oh cool, ich auch :D" + Environment.NewLine + "Guck mal, das hab ich gerade gefunden:" + Environment.NewLine + " [Katzen_Meme] xD" + Environment.NewLine + Environment.NewLine;
                                        _btn_option1.Content = "Haha xD";
                                        _btn_option2.Content = "Aww wie süüüüß :D";
                                        _btn_option3.Visibility = Visibility.Hidden;

                                        if (decisionsScenario2.Count >= 4)
                                        {
                                            //Entscheidung 4
                                            switch (decisionsScenario2[3])
                                            {
                                                case 1:
                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;

                                                    break;

                                                case 2:
                                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;

                                                    break;


                                            }

                                            chat.Text += chatpartner + "Oder?:DD" + Environment.NewLine + "Was hast du heute noch so vor?" + Environment.NewLine + Environment.NewLine;
                                            _btn_option1.Content = "Muss noch Mathe‐Hausaufgaben machen… :(";
                                            _btn_option2.Content = "Mich mit Freunden treffen";
                                            _btn_option3.Visibility = Visibility.Visible;
                                            _btn_option3.Content = "Nicht viel, noch bisschen chatten undso.. Und du so?";

                                            if (decisionsScenario2.Count >= 5)
                                            {
                                                //Entscheidung 5
                                                switch (decisionsScenario2[4])
                                                {
                                                    case 1:
                                                        information++;
                                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Wenns nicht Mathe wäre, würd ich dir meine Hilfe anbieten" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += user + "Ach alles gut, ich schaff das schon irgendwie. Zur not schreib ich morgen bei meinen Freunden ab :D" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Das hab ich an meiner alten Schule auch immer gemacht haha" + Environment.NewLine + "Aber jetzt muss ich meine Hausaufgaben immer selber machen xD" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += user + "Haha xD Das muss ja doof sein hahaIch mach jetzt besser meine Hausaufgaben, bevor meine Mutter wütend wird" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                        HideOptions();
                                                        ShowEnding();
                                                        break;
                                                }
                                            }
                                        }

                                        break;

                                    case 2:
                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "Oh ok... Wollts nur wissen, weil ich ehrlicher Weise ungern mit viel jüngeren oder älteren schreiben will...fänd ich komisch..." + Environment.NewLine + Environment.NewLine;
                                        _btn_option1.Content = "Ja ich auch... Aber ich möchte mein Alter ungern teilen";
                                        _btn_option2.Visibility = Visibility.Hidden;
                                        _btn_option3.Visibility = Visibility.Hidden;

                                        if (decisionsScenario2.Count >= 4)
                                        {
                                            //Entscheidung 4.2
                                            switch (decisionsScenario2[3])
                                            {
                                                case 1:
                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                    chat.Text += chatpartner + "Oh ok… Wenn du mir nicht sagst, wie alt du bist, will ich nicht mit dir schreiben:( " + Environment.NewLine + Environment.NewLine;
                                                    HideOptions();
                                                    ShowEnding();
                                                    break;
                                            }
                                        }
                                        break;
                                }

                            }
                            break;

                        case 2:
                            information++;
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "Oh ja, ich auch! Ansonsten spiel ich auch oft Hockey in meiner Freizeit" + Environment.NewLine + "Darf ich fragen, wie alt du bist?" + Environment.NewLine + Environment.NewLine;
                            _btn_option1.Content = $"Ich bin {Start.user.Age}!";
                            _btn_option2.Content = "Das geht dich nichts an...";
                            _btn_option3.Visibility = Visibility.Hidden;

                            if (decisionsScenario2.Count >= 3)
                            {
                                //Entscheidung 3.2

                                switch (decisionsScenario2[2])
                                {
                                    case 1:
                                        hasAge = true;
                                        information++;
                                        information++;
                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "Oh cool, ich auch :D" + Environment.NewLine + "Guck mal, das hab ich gerade gefunden:" + Environment.NewLine + " [Katzen_Meme] xD" + Environment.NewLine + Environment.NewLine;
                                        _btn_option1.Content = "Haha xD";
                                        _btn_option2.Content = "Aww wie süüüüß :D";
                                        _btn_option3.Visibility = Visibility.Hidden;

                                        if (decisionsScenario2.Count >= 4)
                                        {
                                            //Entscheidung 4.2
                                            switch (decisionsScenario2[3])
                                            {
                                                case 1:
                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;

                                                    break;

                                                case 2:
                                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;

                                                    break;
                                            }
                                            chat.Text += chatpartner + "Oder?:DD" + Environment.NewLine + "Was hast du heute noch so vor?" + Environment.NewLine + Environment.NewLine;
                                            _btn_option1.Content = "Muss noch Mathe‐Hausaufgaben machen… :(";
                                            _btn_option2.Content = "Mich mit Freunden treffen";
                                            _btn_option3.Visibility = Visibility.Visible;
                                            _btn_option3.Content = "Nicht viel, noch bisschen chatten und so.. Und du so?";

                                            if (decisionsScenario2.Count >= 5)
                                            {
                                                //Entscheidung 5.2
                                                switch (decisionsScenario2[4])
                                                {
                                                    case 1:
                                                        information++;
                                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Wenns nicht Mathe wäre, würd ich dir meine Hilfe anbieten" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += user + "Ach alles gut, ich schaff das schon irgendwie. Zur not schreib ich morgen bei meinen Freunden ab :D" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Das hab ich an meiner alten Schule auch immer gemacht haha" + Environment.NewLine + "Aber jetzt muss ich meine Hausaufgaben immer selber machen xD" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += user + "Haha xD Das muss ja doof sein hahaIch mach jetzt besser meine Hausaufgaben, bevor meine Mutter wütend wird" + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                        HideOptions();
                                                        ShowEnding();
                                                        break;

                                                    case 2:
                                                        information++;
                                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Oh , viel Spaß!" + Environment.NewLine + Environment.NewLine;
                                                        _btn_option1.Content = "Danke!";
                                                        _btn_option2.Content = "Möchtest du vielleicht mitkommen? Also, falls du in der Nähe wohnst";
                                                        _btn_option3.Visibility = Visibility.Hidden;

                                                        if (decisionsScenario2.Count >= 6)
                                                        {

                                                            //Entscheidung 6.2
                                                            switch (decisionsScenario2[5])
                                                            {
                                                                case 1:
                                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Wann trefft ihr euch denn?" + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += user + "Gleich... deswegen würd ich mich jetzt auch verabschieden" + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Oh ok :o Danke für das kurze Gespräch :D" + Environment.NewLine + "Wollen wir später weiter schreiben?" + Environment.NewLine + Environment.NewLine;
                                                                    _btn_option1.Content = "Gerne ^^ Bis später";
                                                                    _btn_option2.Content = "Lieber nicht...sorry";
                                                                    _btn_option3.Visibility = Visibility.Hidden;

                                                                    if (decisionsScenario2.Count >= 7)
                                                                    {
                                                                        //Entscheidung 7
                                                                        switch (decisionsScenario2[6])
                                                                        {
                                                                            case 1:
                                                                                chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                                HideOptions();
                                                                                ShowEnding();
                                                                                break;

                                                                            case 2:
                                                                                chat.Text += user + _btn_option2.Content;
                                                                                chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                                HideOptions();
                                                                                ShowEnding();
                                                                                break;
                                                                        }
                                                                    }
                                                                    break;

                                                                case 2:
                                                                    information++;
                                                                    information++;
                                                                    information++;
                                                                    wantsToMeet = true;
                                                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Ähh.. Danke für das Angebot, aber ich treff mich doch nicht einfach so mit Fremden aus dem Internet" + Environment.NewLine + Environment.NewLine;
                                                                    _btn_option1.Content = "Ok, Schade!";
                                                                    _btn_option2.Content = "Ja stimmt, das wäre echt doof";
                                                                    _btn_option3.Visibility = Visibility.Hidden;

                                                                    if (decisionsScenario2.Count >= 7)
                                                                    {
                                                                        //Entscheidung 7.2
                                                                        switch (decisionsScenario2[6])
                                                                        {
                                                                            case 1:
                                                                                chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Hm.. Ja." + Environment.NewLine + "Ich gehe jetzt off ^^ Du solltest vielleicht in Zukunft nicht irgendwelche Fremden zu einem Treffen einladen. Das könnten echt irgendwelche creeps sein." + Environment.NewLine + Environment.NewLine;
                                                                                HideOptions();
                                                                                ShowEnding();
                                                                                break;

                                                                            case 2:
                                                                                chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Hm.. Ja." + Environment.NewLine + "Ich gehe jetzt off ^^ Du solltest vielleicht in Zukunft nicht irgendwelche Fremden zu einem Treffen einladen. Das könnten echt irgendwelche creeps sein." + Environment.NewLine + Environment.NewLine;
                                                                                HideOptions();
                                                                                ShowEnding();
                                                                                break;
                                                                        }
                                                                    }

                                                                    break;
                                                            }

                                                        }
                                                        break;

                                                    case 3:
                                                        chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                                                        chat.Text += chatpartner + "Weiß noch nicht, aber ich muss jetzt erstmal essen. Wollen wir später weiter schreiben ? :o" + Environment.NewLine + Environment.NewLine;
                                                        _btn_option1.Content = "Gerne ^^ Bis später";
                                                        _btn_option2.Content = "Lieber nicht...sorry";
                                                        _btn_option3.Visibility = Visibility.Hidden;

                                                        if (decisionsScenario2.Count >= 6)
                                                        {
                                                            //Entscheidung 6.3
                                                            switch (decisionsScenario2[5])
                                                            {
                                                                case 1:
                                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                    HideOptions();
                                                                    ShowEnding();
                                                                    break;

                                                                case 2:
                                                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                    HideOptions();
                                                                    ShowEnding();
                                                                    break;
                                                            }
                                                        }

                                                        break;
                                                }
                                            }
                                        }

                                        break;

                                    case 2:
                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "Oh ok... Wollts nur wissen, weil ich ehrlicher Weise ungern mit viel jüngeren oder älteren schreiben will...fänd ich komisch..." + Environment.NewLine + Environment.NewLine;
                                        _btn_option1.Content = "Ja ich auch... Aber ich möchte mein Alter ungern teilen";
                                        _btn_option2.Content = $"Versteh ich,ja.. Ich bin {Start.user.Age}";
                                        _btn_option3.Visibility = Visibility.Hidden;

                                        if (decisionsScenario2.Count >= 4)
                                        {
                                            //Entscheidung 4.2
                                            switch (decisionsScenario2[3])
                                            {
                                                case 1:
                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                    chat.Text += chatpartner + "Oh ok… Wenn du mir nicht sagst, wie alt du bist, will ich nicht mit dir schreiben:(" + Environment.NewLine + Environment.NewLine;
                                                    HideOptions();
                                                    ShowEnding();
                                                    break;

                                                case 2:
                                                    information++;
                                                    information++;
                                                    hasAge = true;
                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                    chat.Text += chatpartner + "Oh cool, ich auch :D" + Environment.NewLine + "Guck mal, das hab ich gerade gefunden:" + Environment.NewLine + " [Katzen_Meme] xD" + Environment.NewLine + Environment.NewLine;
                                                    _btn_option1.Content = "Haha xD";
                                                    _btn_option2.Content = "Aww wie süüüüß :D";
                                                    _btn_option3.Visibility = Visibility.Hidden;

                                                    if (decisionsScenario2.Count >= 5)
                                                    {
                                                        //Entscheidung 5.2
                                                        switch (decisionsScenario2[4])
                                                        {
                                                            case 1:
                                                                chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;

                                                                break;

                                                            case 2:
                                                                chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;

                                                                break;
                                                        }
                                                        chat.Text += Environment.NewLine + Environment.NewLine + "Oder?:DD" + Environment.NewLine + "Was hast du heute noch so vor?" + Environment.NewLine + Environment.NewLine;
                                                        _btn_option1.Content = "Muss noch Mathe‐Hausaufgaben machen… :(";
                                                        _btn_option2.Content = "Mich mit Freunden treffen";
                                                        _btn_option3.Visibility = Visibility.Visible;
                                                        _btn_option3.Content = "Nicht viel, noch bisschen chatten undso.. Und du so?";

                                                        if (decisionsScenario2.Count >= 6)
                                                        {
                                                            //Entscheidung 6.2
                                                            switch (decisionsScenario2[5])
                                                            {
                                                                case 1:
                                                                    chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Wenns nicht Mathe wäre, würd ich dir meine Hilfe anbieten" + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += user + "Ach alles gut, ich schaff das schon irgendwie. Zur not schreib ich morgen bei meinen Freunden ab :D" + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Das hab ich an meiner alten Schule auch immer gemacht haha" + Environment.NewLine + "Aber jetzt muss ich meine Hausaufgaben immer selber machen xD" + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += user + "Haha xD Das muss ja doof sein haha. Ich mach jetzt besser meine Hausaufgaben, bevor meine Mutter wütend wird" + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Oh ok, alles klar :) " + Environment.NewLine + Environment.NewLine;
                                                                    HideOptions();
                                                                    ShowEnding();
                                                                    break;

                                                                case 2:
                                                                    chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Oh , viel Spaß!" + Environment.NewLine + Environment.NewLine;
                                                                    _btn_option1.Content = "Danke!";
                                                                    _btn_option2.Content = "Möchtest du vielleicht mitkommen? Also, falls du in der Nähe wohnst";
                                                                    _btn_option3.Visibility = Visibility.Hidden;

                                                                    if (decisionsScenario2.Count >= 7)
                                                                    {

                                                                        //Entscheidung 7.2
                                                                        switch (decisionsScenario2[6])
                                                                        {
                                                                            case 1:
                                                                                chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Wann trefft ihr euch denn?" + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += user + "Gleich... deswegen würd ich mich jetzt auch verabschieden" + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Oh ok :o Dabke für das kurze Gespräch :D" + Environment.NewLine + "Wollen wir später weiter schreiben?" + Environment.NewLine + Environment.NewLine;
                                                                                _btn_option1.Content = "Gerne ^^ Bis später";
                                                                                _btn_option2.Content = "Lieber nicht...sorry";
                                                                                _btn_option3.Visibility = Visibility.Hidden; ;

                                                                                if (decisionsScenario2.Count >= 8)
                                                                                {
                                                                                    //Entscheidung 8
                                                                                    switch (decisionsScenario2[7])
                                                                                    {
                                                                                        case 1:
                                                                                            chat.Text += user + _btn_option1.Content;
                                                                                            chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                                            HideOptions();
                                                                                            ShowEnding();
                                                                                            break;

                                                                                        case 2:
                                                                                            chat.Text += user + _btn_option2.Content;
                                                                                            chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                                            HideOptions();
                                                                                            ShowEnding();
                                                                                            break;
                                                                                    }
                                                                                }
                                                                                break;

                                                                            case 2:
                                                                                information++;
                                                                                information++;
                                                                                information++;
                                                                                wantsToMeet = true;
                                                                                chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Ähh.. Danke für das Angebot, aber ich treff mich doch nicht einfach so mit Fremden aus dem Internet" + Environment.NewLine + Environment.NewLine;
                                                                                _btn_option1.Content = "Ok, Schade!";
                                                                                _btn_option2.Content = "Ja stimmt, das wäre echt doof";
                                                                                _btn_option3.Visibility = Visibility.Hidden;

                                                                                if (decisionsScenario2.Count >= 8)
                                                                                {
                                                                                    //Entscheidung 8.2
                                                                                    switch (decisionsScenario2[7])
                                                                                    {
                                                                                        case 1:
                                                                                            information++;
                                                                                            information++;
                                                                                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                                            chat.Text += chatpartner + "Hm.. Ja." + Environment.NewLine + "Ich gehe jetzt off ^^ Du solltest vielleicht in Zukunft nicht irgendwelche Fremden zu einem Treffen einladen. " +
                                                                                                "Das könnten echt irgendwelche creeps sein." + Environment.NewLine + Environment.NewLine;
                                                                                            HideOptions();
                                                                                            ShowEnding();
                                                                                            break;

                                                                                        case 2:
                                                                                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                                            chat.Text += chatpartner + "Hm.. Ja." + Environment.NewLine + "Ich gehe jetzt off ^^ Du solltest vielleicht in Zukunft nicht irgendwelche Fremden zu einem Treffen einladen." +
                                                                                                " Das könnten echt irgendwelche creeps sein." + Environment.NewLine + Environment.NewLine;
                                                                                            HideOptions();
                                                                                            ShowEnding();
                                                                                            break;
                                                                                    }
                                                                                }

                                                                                break;
                                                                        }

                                                                    }
                                                                    break;

                                                                case 3:
                                                                    chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                                                                    chat.Text += chatpartner + "Weiß noch nicht, aber ich muss jetzt erstmal essen. Wollen wir später weiter schreiben ? :o" + Environment.NewLine + Environment.NewLine;
                                                                    _btn_option1.Content = "Gerne ^^ Bis später";
                                                                    _btn_option2.Content = "Lieber nicht...sorry";
                                                                    _btn_option3.Visibility = Visibility.Hidden;

                                                                    if (decisionsScenario2.Count >= 7)
                                                                    {
                                                                        //Entscheidung 7.3
                                                                        switch (decisionsScenario2[6])
                                                                        {
                                                                            case 1:
                                                                                chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                                HideOptions();
                                                                                ShowEnding();
                                                                                break;

                                                                            case 2:
                                                                                chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                                                                chat.Text += chatpartner + "Oh ok, alles klar :)" + Environment.NewLine + Environment.NewLine;
                                                                                HideOptions();
                                                                                ShowEnding();
                                                                                break;
                                                                        }
                                                                    }

                                                                    break;
                                                            }
                                                        }
                                                    }

                                                    break;
                                            }
                                        }
                                        break;
                                }

                            }
                            break;
                    }
                }
            }
            chat.ScrollToEnd();
        }

        private void DisplayScenario3()
        {
            _btn_option1.Visibility = Visibility.Visible; 
            _btn_option2.Visibility = Visibility.Visible;
            _btn_option3.Visibility = Visibility.Hidden;
            _btn_option1.Content = "Gut und dir?";
            _btn_option2.Content = "Ganz oke. Und du so?";
           
            string chatpartner = _chatpartner_name.Text + ":" + Environment.NewLine;
            string user = "Du:" + Environment.NewLine;
            chat.Text = chatpartner + "Hey :)" + Environment.NewLine + Environment.NewLine;
            chat.Text += user + "Hey :)" + Environment.NewLine + Environment.NewLine;
            chat.Text += chatpartner + "Wie geht's?" + Environment.NewLine + Environment.NewLine;
    

            //Entscheidung 1
            if (decisionsScenario3.Count > 0)
            {
                switch (decisionsScenario3[0])
                {
                    case 1:
                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                        break;
                    case 2:
                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                        break;
                }
            }

            if (decisionsScenario3.Count >= 1)
            {
                chat.Text += chatpartner + "Auch. Du gehst auch auf die Schule am Marktplatz, oder nicht?" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Ja genau, du auch?";
                _btn_option2.Content = "Äh.. woher weißt du das?";
                _btn_option3.Visibility = Visibility.Visible;
                _btn_option3.Content = "Sag ich dir doch nicht";

                //Entscheidung 2
                if (decisionsScenario3.Count >= 2)
                {
                    switch (decisionsScenario3[1])
                    {
                        case 1:
                            hasSchool = true;
                            information++;
                            information++;
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "Ne, ich geh auf eine andere Schule, hab dein Profil gesehen, weil du mit Lena befreundet bist" + Environment.NewLine + Environment.NewLine;
                            break;
                        case 2:
                            hasSchool = true;
                            information++;
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "Ich hab dein Profil gesehen, weil du mit Chris befreundet bist... Keine Sorge, bin doch kein komischer Typ :) Ich geh jedenfalls auf eine andere Schule." + Environment.NewLine + Environment.NewLine;
                            break;
                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "Ich hab dein Profil gesehen, weil du mit Chris befreundet bist... Keine Sorge, bin doch kein komischer Typ :) Ich geh jedenfalls auf eine andere Schule." + Environment.NewLine + Environment.NewLine;
                            break;
                    }
                }
            }

            if (decisionsScenario3.Count >= 2)
            {
                _btn_option1.Content = "Alles klar";
                _btn_option2.Content = "Aha.. ok";
                _btn_option3.Content = "Kenn dich nicht";
                if (decisionsScenario3.Count >= 3)
                {
                    //Entscheidung 3
                    switch (decisionsScenario3[2])
                    {
                        case 1:

                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            break;
                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            break;
                        case 3:
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            break;
                    }
                }
            }

            if (decisionsScenario3.Count >= 3)
            {
                chat.Text += chatpartner + "Bist wohl nicht so gesprächig.. Bist du schüchtern? Versteh ich voll. Bin ja auch ein Fremder ^^" + Environment.NewLine + Environment.NewLine;
                _btn_option1.Content = "Ich bin nicht schüchtern...";
                _btn_option2.Content = "Kann sein";
                _btn_option3.Content = "Tut mir leid";
                if (decisionsScenario3.Count >= 4)
                {
                    //Entscheidung 4
                    switch (decisionsScenario3[3])
                    {
                        case 1:
                            information++;
                            chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                            if (decisionsScenario3.Count == 4)
                            {
                                decisionsScenario3.Add(666);
                            }
                            break;

                        case 2:
                            chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                            chat.Text += chatpartner + "Ach, das brauchst du nicht sein, keine Sorgen, ich beiße nicht ;)" + Environment.NewLine + Environment.NewLine;
                            _btn_option1.Content = "Haha";
                            _btn_option2.Content = "Ew";
                            _btn_option3.Visibility = Visibility.Hidden;

                            if (decisionsScenario3.Count >= 5)
                            {
                                //Entscheidung 5
                                switch (decisionsScenario3[4])
                                {
                                    case 1:
                                        chat.Text += user + _btn_option1.Content + Environment.NewLine + Environment.NewLine;
                                        break;
                                    case 2:
                                        chat.Text += user + _btn_option2.Content + Environment.NewLine + Environment.NewLine;
                                        chat.Text += chatpartner + "..." + Environment.NewLine + Environment.NewLine;
                                        Ending ending = new Ending();
                                        ending.Show();
                                        Close();
                                        break;
                                }
                            }
                            break;

                        case 3:
                            information++;
                            chat.Text += user + _btn_option3.Content + Environment.NewLine + Environment.NewLine;
                            if (decisionsScenario3.Count == 4)
                            {
                                decisionsScenario3.Add(666);
                            }
                            break;
                    }
                }

                if (decisionsScenario3.Count >= 5)
                {
                    chat.Text += chatpartner + "Ich hab gesehen, dass in deinem Profil steht, dass du gerne Fahrrad fährst. Fährst du auch immer mit dem Rad zur Schule?" + Environment.NewLine + Environment.NewLine;

                }
            }
            chat.ScrollToEnd();
        }

        private void Scenario1_Click(object sender, RoutedEventArgs e)
        {
            _chatpartner_name.Text = _btn_scenario1.Content.ToString();
            DisplayScenario1();
            if (isScenario1Blocked)
            {
                Block();
            }
            else
            {
                Unblock();
            }
        }

        private void Scenario2_Click(object sender, RoutedEventArgs e)
        {
            _chatpartner_name.Text = _btn_scenario2.Content.ToString();
            DisplayScenario2();
            if (isScenario2Blocked)
            {
                Block();
            }
            else
            {
                Unblock();
            }
        }

        private void Scenario3_Click(object sender, RoutedEventArgs e)
        {
            _chatpartner_name.Text = _btn_scenario3.Content.ToString();
            DisplayScenario3();
            if (isScenario3Blocked)
            {
                Block();
            }
            else
            {
                Unblock();
            }
        }

        private void Option1_Click(object sender, RoutedEventArgs e)
        {
            if (_chatpartner_name.Text == "Lena")
            {
                decisionsScenario1.Add(1);
                DisplayScenario1();
            }
            else if (_chatpartner_name.Text == "Chris")
            {
                decisionsScenario2.Add(1);
                DisplayScenario2();
            }
            else if (_chatpartner_name.Text == "Pedro")
            {
                decisionsScenario3.Add(1);
                DisplayScenario3();
            }
        }

        private void Option2_Click(object sender, RoutedEventArgs e)
        {
            //answers.Text += Environment.NewLine + Environment.NewLine + _btn_option2.Content;
            if (_chatpartner_name.Text == "Lena")
            {
                decisionsScenario1.Add(2);
                DisplayScenario1();
            }
            else if (_chatpartner_name.Text == "Chris")
            {
                decisionsScenario2.Add(2);
                DisplayScenario2();
            }
            else if (_chatpartner_name.Text == "Pedro")
            {
                decisionsScenario3.Add(2);
                DisplayScenario3();
            }
        }

        private void Option3_Click(object sender, RoutedEventArgs e)
        {
            //answers.Text += Environment.NewLine + Environment.NewLine + _btn_option3.Content;
            if (_chatpartner_name.Text == "Lena")
            {
                decisionsScenario1.Add(3);
                DisplayScenario1();
            }
            else if (_chatpartner_name.Text == "Chris")
            {
                decisionsScenario2.Add(3);
                DisplayScenario2();
            }
            else if (_chatpartner_name.Text == "Pedro")
            {
                decisionsScenario3.Add(3);
                DisplayScenario3();
            }
        }

        private void ReloadClick(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            List<int> list = new List<int>();
            hasAge = false;
            hasBlocked = false;
            hasName = false;
            hasSchool = false;
            wantsToMeet = false;
            information = 0;
            decisionsScenario1.Clear();
            decisionsScenario2.Clear();
            decisionsScenario3.Clear();
            isScenario1Blocked = false;
            isScenario2Blocked = false;
            isScenario3Blocked = false;
        }

        private void HideOptions()
        {
            _btn_option1.Visibility = Visibility.Hidden;
            _btn_option2.Visibility = Visibility.Hidden;
            _btn_option3.Visibility = Visibility.Hidden;
        }

        private void ShowOptions()
        {
            _btn_option1.Visibility = Visibility.Visible;
            _btn_option2.Visibility = Visibility.Visible;
            _btn_option3.Visibility = Visibility.Visible;
        }

        private void blockUser_Click(object sender, RoutedEventArgs e)
        {
            //Nur jeweiligen User blockieren To do 
            hasBlocked = true;


            if (_chatpartner_name.Text == "Lena")
            {
                isScenario1Blocked = !isScenario1Blocked;

                if (isScenario1Blocked)
                {
                    Block();
                }
                else
                {
                    Unblock();
                }
            }

            else if (_chatpartner_name.Text == "Chris")
            {
                isScenario2Blocked = !isScenario2Blocked;

                if (isScenario2Blocked)
                {
                    Block();
                }
                else
                {
                    Unblock();
                }
            }

            else
            {
                isScenario3Blocked = !isScenario3Blocked;

                if (isScenario3Blocked)
                {
                    Block();
                }
                else
                {
                    Unblock();
                }
            }
        }

        private void Block()
        {
            HideOptions();
            _userBlocked.Visibility = Visibility.Visible;
            _btn_blockUser.Text = "Entblockieren";
        }

        private void Unblock()
        {
            ShowOptions();
            _userBlocked.Visibility = Visibility.Hidden;
            _btn_blockUser.Text = "Blockieren";
        }

        private void ShowEnding()
        {
            Ending ending = new Ending();
            ending.Show();
            Close();

        }
    }
}
